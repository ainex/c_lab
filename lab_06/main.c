#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "fileService.h"

void extern removeNotLatinStartStrings(FILE *input, int limit);

void extern addStringTrailingSymbols(FILE *input, char symbol);

void bindArgs(int argc, char* argv[], char fileName[], FILE **fp, int *mode, int *linesAmount, char *symbol );
int convertToInt(char arg[]);

int main(int argc, char* argv[])
{
    char fileName[100];
    FILE *fp = NULL;
    int mode, linesAmount;
    char symbol;

    //bind arguments
    bindArgs(argc, argv, fileName, &fp, &mode, &linesAmount, &symbol );
    if(mode == 1)
    {
        //convert to lines
        removeNotLatinStartStrings(fp, linesAmount);
        printf("\nAll strings starting with a non Latin symbol were removed. \nSee an result in a 'output_1.txt' file");
    }
    else if(mode == 2)
    {
        //convert to symbols
        addStringTrailingSymbols(fp, symbol);
        printf("\nThe trailing symbol \'%c\' was added for all lines. \nSee an result in a 'output_2.txt' file", symbol);
    }
    else
    {
        printf("\nWrong program mode, terminated.");
        exit(0);
    }

    return 0;
}

void bindArgs(int argc, char* argv[], char fileName[], FILE **fp, int *mode, int *linesAmount, char *symbol)
{
    if(argc != 4)
    {
        printf("\nPlease run the program with three arguments:\n1. a file name \n2. a mode 1 or 2 and \n3. a number of lines(1) or a symbol(2) according to the program mode.\n");
        exit(0);
    }

    *mode = convertToInt(argv[2]);
    printf("\nProgram mode is %d", *mode);

    *fp = fopen(argv[1], "r");
    if (NULL !=*fp)printf("\nfile %s is open", argv[1]);

    if (*mode == 1)
    {
        *linesAmount = convertToInt(argv[3]);
        printf("\nlinesAmount = %d", *linesAmount);
    }
    if (*mode == 2)
    {
        *symbol = argv[3][0];
        printf("\nsymbol = %c", *symbol);
    }

}

int convertToInt(char arg[])
{
    int result;
    errno = 0;
    result = (int)strtoimax(arg,NULL,10);

    if (errno != 0)
    {
        // Could not convert
        printf("Wrong argument %s; %s\n", arg, strerror(errno));
        printf("errno=%d", errno);
        exit(0);
    }
    else
    {
        return result;
    }
}
//3.1. �������� ���������, �������������� ��������� ���� � ������������ ������������ ������ � ���� � ����� �� ������, �� �
//������ ����� (����. 7).
/* �������� ������ ������, ������������ � ���������
����
1.1. ��� �������� �����
2.2. ���������� ��������������
����� */
//3.2. �������� ���������, ����������� ������������ ���������
//���������� ����� (����. 8).
//���� ���������� ������������ � ��������� ������ ������� ���������.

/* � ����� ������ ������ �������� �������� ������
1.1. ��� �������� �����
2.2. �������� ������ */
