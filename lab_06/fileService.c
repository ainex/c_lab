#include "fileService.h"

void removeNotLatinStartStrings(FILE *input, int limit)
{
    int bufSize = 30;
    char buf[bufSize], *n;
    FILE *output = fopen("output_1.txt", "w");
    //end of string in file '\n' + '\0'
    int endOfLine = 0;
    while(n = fgets(buf, bufSize, input) != NULL && limit != 0)
    {
        limit--;
        if(isLat(buf[0]))
        {
            //copy till end of line or file
            do
            {
                fputs(buf, output);
                endOfLine = isEoL(buf, bufSize);
                if(!endOfLine)
                {
                    n = fgets(buf, bufSize, input);
                }

            }
            while(  n != NULL  && !endOfLine);
        }
        else
        {
            //skip till end of line
            while(!isEoL(buf, bufSize))
            {
                fgets(buf, bufSize, input);

            }
        }

    }
    fclose(output);
    fclose(input);
}
void addStringTrailingSymbols(FILE *input, char symbol)
{
    int bufSize = 40;
    char buf[bufSize+1]; //one more for the symbol
    FILE *output = fopen("output_2.txt", "w");
    //end of string in file '\n' + '\0'
    while(fgets(buf, bufSize, input) != NULL)
    {
        modifyChunk(buf, bufSize, symbol);
        fputs(buf, output);

    }
    fclose(output);
    fclose(input);
}

int  isLat(char x)
{
    return (x >= 'a' && x <= 'z') || (x >= 'A' && x <= 'Z');
}
int isEoL(char line[], int length)
{
    int j=0;
    for(j =0; j < length; j++)
    {
        if(line[j]== '\n') return 1;
    }
    return 0;
}

void modifyChunk(char buf[], int bufSize, char symbol)
{
    int j=0;
    for(j =0; j < bufSize; j++)
    {
        if(buf[j]== '\n')
        {
            buf[j+2] = buf[j+1];
            buf[j+1] = buf[j];
            buf[j] = symbol;
            return;
        };
    }

}
