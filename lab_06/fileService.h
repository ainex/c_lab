#include <stdio.h>
#include <stdlib.h>

void removeNotLatinStartStrings(FILE *input, int limit);

void addStringTrailingSymbols(FILE *input, char symbol);

int  isLat(char x);

int isEoL(char line[], int length);

void modifyChunk(char buf[], int bufSize, char symbol);
