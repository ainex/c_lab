#include "detailService.h"
#include "dao.h"

extern void quikSortDetailsByPrice();
Detail *getNewDetail()
{
    Detail *d = (Detail*)malloc(sizeof(Detail));
    printf("Enter a new detail description:\n");
    printf("name: ");
    scanf("%s", d->name);
    printf("year of manufacture: ");
    scanf("%d", &(d->year));
    printf("price: ");
    scanf("%f", &(d->price));
    printf("amount: ");
    scanf("%d", &(d->amount));
    return d;
};

void addDetailRecord()
{
    int recordIndex = create(getNewDetail());
    printf("\nNew record was added");
    show(recordIndex);
    printf("\n");
}

void printTableDetails()
{
    showAll();
};

void tableDetailsSortByPrice()
{
    printf("\n sort table Details by price \n");
    quikSortDetailsByPrice();
    printf("\n finish sort table Details by price \n");
}
