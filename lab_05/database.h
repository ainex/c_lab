#ifndef DATABASE_H
#define DATABASE_H

struct Detail
{
    char name[250];
    int year;
    float price;
    int amount;
};

struct Database
{
    Detail *tableDetails[100];
    int dSize = 0;
};


static Database database;

void quikSortDetailsByPrice();
void qs(Detail *table[], int first, int last);
void swapP(Detail **p1, Detail **p2);
#endif

