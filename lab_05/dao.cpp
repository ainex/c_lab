#include <stdio.h>
#include <stdlib.h>
#include "dao.h"

extern Database database;

int create(Detail *detail)
{
    int recordIndex = database.dSize;
    database.tableDetails[recordIndex] = detail;
    database.dSize++;
    return recordIndex;
};

void show(int index)
{
    Detail *detail = database.tableDetails[index];
    printf("\n[%d] \t%s \t%d \t%5.2f \t%d", index, detail->name, detail->year, detail->price, detail->amount);
}

void showAll()
{
    if(database.dSize < 1)
    {
        printf("\nThe tableDetails is empty \n");
        return;
    }
    printf("\n# \tNAME \tYEAR \tPRICE \tAMOUNT");
    for (int i = 0; i<database.dSize; i++)
    {
        show(i);
    }
    printf("\n");
}

void quikSortDetailsByPrice()
{
    printf("db size %d", database.dSize);
    qs(database.tableDetails, 0, database.dSize-1);

}

