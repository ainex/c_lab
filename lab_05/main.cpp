#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "detailService.h"

void add(char command[]);
void print(char command[]);
void sortTable(char command[]);
void help(char command[]);
void quit(char command[]);


extern void addDetailRecord();
extern void printTableDetails();
extern void tableDetailsSortByPrice();

struct resolver
{
    const char *cmd;
    void (*resolve_func)(char*);
};


struct resolver table[] =
{
    { "add",  add },
    { "print", print },
    { "help", help },
    { "exit", quit },
    { "sort", sortTable },
    { NULL, NULL }
};
char infoMessage[] = "Type \n \'add\' to enter data \n \'print\' to print data \n \'sort\' to sort data \n \'exit\' to exit the app \n \'help\' to print this message again\n";

int main()
{
    char command[10]  = "";
    printf(infoMessage);
    while(true)
    {
        scanf("%s", &command );
        printf("command %s\n", command);
        int i;
        for ( i=0; table[i].cmd != NULL; i++)
        {
            if (! strcmp (command, table[i].cmd))
            {
                table[i].resolve_func(command);
                break;
            }
        }
        //no command was found
        if(table[i].cmd == NULL)
        {
            printf("No such command \n");
        }

    }
    return 0;
}


void add(char command[])
{
    addDetailRecord();
}


void print(char command[])
{
    printTableDetails();
}

void sortTable(char command[])
{
    tableDetailsSortByPrice();
}

void help(char command[])
{
    printf(infoMessage);
}

void quit(char command[])
{
    exit(0);
}

