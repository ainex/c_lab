#include <stdio.h>
#include <stdlib.h>
#include "database.h"

void qs(Detail *table[], int first, int last)
{
    static float eps = 0.01;
    int i = first, j = last;
    Detail *pivot = table[(first + last) / 2];

    do
    {
        while ( (pivot->price - table[i]->price) > eps) i++;
        while ((table[j]->price - pivot->price) > eps) j--;
        if(i <= j)
        {
            if (table[i]->price - table[j]->price > eps) swapP(&(table[i]), &(table[j]));
            i++;
            j--;
        }
    }
    while (i <= j);

    if (i < last)
        qs(table, i, last);
    if (first < j)
        qs(table, first, j);
}

void swapP(Detail **p1, Detail **p2)
{
    Detail *p = *p1;
    *p1=*p2;
    *p2 =p;
}
