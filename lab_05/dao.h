#ifndef DAO_H
#define DAO_H

#include "database.h"

extern int create(Detail *detail);

extern void show(int index);

extern void showAll();

extern void quikSortDetailsByPrice();

#endif
